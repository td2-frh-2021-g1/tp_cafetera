/*
 * fsm_blink.c
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 */

/*==================[inclusions]=============================================*/

#include "fsm_blink.h"
#include "fsm_cafetera.h"
#include "user_gpio.h"
#include <stdint.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

FSM_BLINK_T blink_1;
FSM_BLINK_T blink_2;

uint32_t cont_500ms;
uint32_t cont_100ms;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(FSM_BLINK_T *handle)
{
	handle->evBlink1Hz_On = false;
	handle->evBlink5Hz_On = false;
	handle->evIndicadorLuz_Off = false;
	handle->evToggle1Hz = false;
	handle->evToggle5Hz = false;
}

static void initBLINK(FSM_BLINK_T *handle)
{
	handle->state = FUNCIONANDO;/*Inicializamos state desde "FUNCIONANDO"*/
	clearEvents(handle);/*Limpiamos los eventos*/
}

static void runCycleBlink(FSM_BLINK_T *handle)
{
	switch (handle->state)
	{
		case FUNCIONANDO:
			if (handle->evBlink5Hz_On)/*Inició la erogación*/
			{
				handle->state = EROGANDO;
				/*No va a entrar en este "If", de la manera que yo implemente el codigo.
				 * Aun asi lo dejo porque es perfectamente posible según las diferentes implementaciones*/
			}
			else if (handle->evIndicadorLuz_Off)/*Estamos esperando elección o ya terminó la erogación*/
			{
				fsm_blink_IndicadorLuz_Off(handle->idblink, 0);/*Apagamos el indicador luminoso*/
				handle->state = APAGADO;
			}
			else if (handle->evToggle1Hz)
			{
				fsm_blink_IndicadorLuz_Toggle(handle->idblink);/*Hacemos togglear el indicador luminoso a 1Hz*/
			}
			break;

		case EROGANDO:
			if (handle->evBlink1Hz_On)/*Esperando ficha*/
			{
				handle->state = FUNCIONANDO;
				/*No va a entrar en este "If", de la manera que yo implemente el codigo.
				 * Aun asi lo dejo porque es perfectamente posible según las diferentes implementaciones*/
			}
			else if (handle->evIndicadorLuz_Off)/*Estamos esperando elección o ya terminó la erogación*/
			{
				fsm_blink_IndicadorLuz_Off(handle->idblink, 0);/*Apagamos el indicador luminoso*/
				handle->state = APAGADO;
			}
			else if (handle->evToggle5Hz)
			{
				fsm_blink_IndicadorLuz_Toggle(handle->idblink);/*Hacemos togglear el indicador luminoso a 5Hz*/
			}
			break;

		case APAGADO:
			if (handle->evBlink1Hz_On)/*Esperando ficha*/
			{
				handle->state = FUNCIONANDO;
			}
			else if (handle->evBlink5Hz_On)/*Inicia la erogación*/
			{
				handle->state = EROGANDO;
			}
			break;
	}

	clearEvents(handle);
}

/*==================[external functions definition]==========================*/

void fsm_blink_init(void)
{
	blink_1.idblink = FSM_1;
	initBLINK(&blink_1);

	blink_2.idblink = FSM_2;
	initBLINK(&blink_2);
}
void fsm_blink_loop(void)
{
	runCycleBlink(&blink_1);
	runCycleBlink(&blink_2);
}


void fsm_blink_tick(void)/*Funcion llamada por HAL_IncTick para realizar los conteos*/
{
	cont_500ms ++;
	cont_100ms ++;

	if (cont_500ms >= CONT_500ms)/*Contamos hasta 500ms para togglear*/
	{
		cont_500ms = INICIAL_CONT;
		blink_1.evToggle1Hz = true;
		blink_2.evToggle1Hz = true;
	}

	if (cont_100ms >= CONT_100ms)/*Contamos hasta 100ms para togglear*/
	{
		cont_100ms = INICIAL_CONT;
		blink_1.evToggle5Hz = true;
		blink_2.evToggle5Hz = true;
	}
}

void fsm_blink_raise_evBlink1Hz_On(uint32_t id)
{
	switch (id) {
	case FSM_1:
		blink_1.evBlink1Hz_On = true;
		break;
	case FSM_2:
		blink_2.evBlink1Hz_On = true;
		break;
	}
}

void fsm_blink_raise_evBlink5Hz_On(uint32_t id)
{
	switch (id) {
	case FSM_1:
		blink_1.evBlink5Hz_On = true;
		break;
	case FSM_2:
		blink_2.evBlink5Hz_On = true;
		break;
	}
}

void fsm_blink_raise_evIndicadorLuz_Off(uint32_t id)
{
	switch (id) {
	case FSM_1:
		blink_1.evIndicadorLuz_Off = true;
		break;
	case FSM_2:
		blink_2.evIndicadorLuz_Off = true;
		break;
	}
}

/*==================[end of file]============================================*/
