/*
 * fonts.h
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 *	---------------------------------------------------------------------
 *
 * 	Copyright (C) Tilen Majerle, 2015		(Original Author)
 *  Copyright (C) Alexander Lutsai, 2016	(Modification for STM32f10x)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  ---------------------------------------------------------------------
 */

#ifndef INC_FONTS_H_
#define INC_FONTS_H_

/**
 *
 * Librería de fuentes predeterminada usada en todos las librerías LCD
 *
 * \par Formatos permitidos
 *
 *  - 7 x 10 pixeles
 *  - 11 x 18 pixeles
 *  - 16 x 26 pixeles
 */

/*==================[inclusions]=============================================*/

#include "stm32f1xx_hal.h"
#include "string.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern C {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/** @addtogroup fonts FONTS
 *			Módulo para las fuentes utilizadas para el OLED ssd1306
 * @{
 */


/**
 * @defgroup lib_typedefs LIB_Typedefs
 * @brief    Typedefs de la librerìa
 * @{
*/

/**
 * @brief  Estructura de la fuente
 */

typedef struct {
	uint8_t FontWidth;    /*!< Ancho de la fuente en píxeles */
	uint8_t FontHeight;   /*!< Alto de la fuente en píxeles */
	const uint16_t *data; /*!< Puntero que apunta hacia los datos de la fuente */
} FontDef_t;

/**
 * @brief  Alto y largo de los string
 */
typedef struct {
	uint16_t Length;      /*!< Ancho de los string en pixeles */
	uint16_t Height;      /*!< Alto de los string en pixeles */
} FONTS_SIZE_t;

/** @} doxygen end group definition */

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/**
 * @defgroup fonts_fontvariables FONTS_FontVariables
 * @brief    Librerìa de fuentes disponibles
 * @{
 */

/**
 * @brief  Fuente de 7 x 10 píxeles
 */
extern FontDef_t Font_7x10;

/**
 * @brief  Fuente de 11 x 18 píxeles
 */
extern FontDef_t Font_11x18;

/**
 * @brief  Fuente de 16 x 26 píxeles
 */
extern FontDef_t Font_16x26;

/** @} doxygen end group definition */

/**
 * @defgroup fonts_functions FONTS_Functions
 * @brief    Librerìa de funciones
 * @{
 */

/**
 * @brief  Calcula el largo y alto de los string en píxeles, segùn el tamaño de fuente utilizado
 * @param  *str: String a revisar
 * @param  *SizeStruct: Puntero que apunta a estructura vacía donde se guardará la información; @ref FONTS_SIZE_t
 * @param  *Font: Puntero que apunta a @ref FontDef_t, la cual contiene el tipo de fuente utilizado
 * @retval Puntero que apunta al string utlizado para largo y alto
 */
char* FONTS_GetStringSize(char* str, FONTS_SIZE_t* SizeStruct, FontDef_t* Font);

/** @} doxygen end group definition */

/** @} doxygen end group definition */

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/

#endif
