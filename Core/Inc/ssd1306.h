/*
 * ssd1306.h
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 *	---------------------------------------------------------------------
 *
 * 	Copyright (C) Tilen Majerle, 2015		(Original Author)
 *  Copyright (C) Alexander Lutsai, 2016	(Modification for STM32f10x)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  ---------------------------------------------------------------------
 */

#ifndef INC_SSD1304_H_
#define INC_SSD1304_H_

/** @addtogroup ssd1306 SSD1306
 *			Este módulo se encarga de presentar en la pantalla OLED los diferentes estados de la cafetera
 * @{
 * @note El dispositivo utiliza I2C para comunicarse
 *
 */

/*==================[inclusions]=============================================*/

#include "stm32f1xx_hal.h"
#include "fonts.h"
#include "stdlib.h"
#include "string.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern C {
#endif

/*==================[macros]=================================================*/

/* Dirección I2C */
#ifndef SSD1306_I2C_ADDR
#define SSD1306_I2C_ADDR         0x78
//#define SSD1306_I2C_ADDR       0x7A
#endif


/* Largo de la pantalla en píxeles */
#ifndef SSD1306_WIDTH
#define SSD1306_WIDTH            128
#endif
/* Alto de la pantalla en píxeles */
#ifndef SSD1306_HEIGHT
#define SSD1306_HEIGHT           64
#endif

/*==================[typedef]================================================*/

/**
 * @brief  Enumaración de colores
 */
typedef enum {
	SSD1306_COLOR_BLACK = 0x00, /*!< Sin color */
	SSD1306_COLOR_WHITE = 0x01  /*!< Pixel encendido, el color depende del modelo de OLED */
} SSD1306_COLOR_t;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/*
 * @brief	Función que inicializa el display
 */
void lcd_init(void);

/*
 * @brief	Función que imprime mensajes en el display
 */
int32_t lcd_print(char * data, 	uint32_t id);

/*
 * @brief	Función que ubica el mensaje en el display
 */
int32_t lcd_goto_xy(uint8_t x, uint8_t y);

/**
 * @brief  Inicialización del ssd1306
 * @retval Estado de inicialización:
 *           - 0: No se detecta el display a través del I2C
 *           - > 0: Display inicializado y listo
 */
uint8_t SSD1306_Init(void);

/**
 * @brief  Actualización del buffer interno del display
 * @note   Esta función debe llamarse cada vez que se envían datos para actualizar la pantalla
 */
void SSD1306_UpdateScreen(void);

/**
 * @brief  ????????????????? Toggles pixels invertion inside internal RAM
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 */
void SSD1306_ToggleInvert(void);

/**
 * @brief  Cubre toda la pantalla en un color sólido
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  Color: Color seleccionado desde @ref SSD1306_COLOR_t
 */
void SSD1306_Fill(SSD1306_COLOR_t Color);

/**
 * @brief  Dibuja un pixel en la coordenada seleccionada
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  x: coordenada en X. Puede ir desde 0 hasta SSD1306_WIDTH - 1
 * @param  y: coordenada en Y. Puede ir desde 0 hasta SSD1306_HEIGHT - 1
 * @param  color: Color seleccionado desde @ref SSD1306_COLOR_t
 */
void SSD1306_DrawPixel(uint16_t x, uint16_t y, SSD1306_COLOR_t color);

/**
 * @brief  Posiciona el cursor en las coodenadas seleccionadas
 * @param  x: X location. This parameter can be a value between 0 and SSD1306_WIDTH - 1
 * @param  y: Y location. This parameter can be a value between 0 and SSD1306_HEIGHT - 1
 */
void SSD1306_GotoXY(uint16_t x, uint16_t y);

/**
 * @brief  Envìa el caracter a la memoria interna del display
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  ch: Caracter a escribir
 * @param  *Font: Puntero que apunta a la estructura @ref FontDef_t
 * @param  color: Color seleccionado desde @ref SSD1306_COLOR_t
 * @retval Caracter escrito
 */
char SSD1306_Putc(char ch, FontDef_t* Font, SSD1306_COLOR_t color);

/**
 * @brief  Envìa el string a la memoria interna del display
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  *str: Caracter a escribir
 * @param  *Font: Puntero que apunta a la estructura @ref FontDef_t
 * @param  color: Color seleccionado desde @ref SSD1306_COLOR_t
 * @retval "0" para éxito o el valor del caracter en caso contrario
 */
char SSD1306_Puts(char* str, FontDef_t* Font, SSD1306_COLOR_t color);



/** @addtogroup ssd1306test Funciones de test del display
 *			se
 * @{
 */

/**
 * @brief  Dibuja una linea
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  x0: Valor x inicial entre 0 y SSD1306_WIDTH - 1
 * @param  y0: Valor y inicial entre 0 y SSD1306_HEIGHT - 1
 * @param  x1: Valor x final entre 0 y SSD1306_WIDTH - 1
 * @param  y1: Valor y final entre 0 y SSD1306_HEIGHT - 1
 * @param  c: Color seleccionado desde @ref SSD1306_COLOR_t
 * @retval None
 */
void SSD1306_DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, SSD1306_COLOR_t c);

/**
 * @brief  Dibuja un rectángulo
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  x: Valor x inicial (arriba la izquierda) entre 0 y SSD1306_WIDTH - 1
 * @param  y: Valor y inicial (arriba la izquierda) entre 0 to SSD1306_HEIGHT - 1
 * @param  w: Largo del rectángulo
 * @param  h: Altura del rectángulo
 * @param  c: Color seleccionado desde @ref SSD1306_COLOR_t
 */
void SSD1306_DrawRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOR_t c);

/**
 * @brief  Dibuja un rectángulo relleno
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  x: Valor x inicial (arriba la izquierda) entre 0 y SSD1306_WIDTH - 1
 * @param  y: Valor y inicial (arriba la izquierda) entre 0 to SSD1306_HEIGHT - 1
 * @param  w: Largo del rectángulo
 * @param  h: Altura del rectángulo
 * @param  c: Color seleccionado desde @ref SSD1306_COLOR_t
 * @retval None
 */
void SSD1306_DrawFilledRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOR_t c);

/**
 * @brief  Dibuja un triángulo
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  x1: Primera coordenada en X entre 0 y SSD1306_WIDTH - 1
 * @param  y1: Primera coordenada en Y entre 0 y SSD1306_HEIGHT - 1
 * @param  x2: Segunda coordenada en X entre 0 y SSD1306_WIDTH - 1
 * @param  y2: Segunda coordenada en Y entre 0 y SSD1306_HEIGHT - 1
 * @param  x3: Tercera coordenada en X entre 0 y SSD1306_WIDTH - 1
 * @param  y3: Tercera coordenada en Y entre 0 y SSD1306_HEIGHT - 1
 * @param  color: Color seleccionado desde @ref SSD1306_COLOR_t
 */
void SSD1306_DrawTriangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, SSD1306_COLOR_t color);

/**
 * @brief  Dibuja un círculo
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  x0: coordenada X para el centro entre 0 y SSD1306_WIDTH - 1
 * @param  y0: coordenada Y para el centro entre 0 y SSD1306_HEIGHT - 1
 * @param  r: radio del círculo
 * @param  c: Color seleccionado desde @ref SSD1306_COLOR_t
 */
void SSD1306_DrawCircle(int16_t x0, int16_t y0, int16_t r, SSD1306_COLOR_t c);

/**
 * @brief  Dibuja un círculo relleno
 * @note   @ref SSD1306_UpdateScreen() debe llamarse para mostrar los cambios
 * @param  x0: coordenada X para el centro entre 0 y SSD1306_WIDTH - 1
 * @param  y0: coordenada Y para el centro entre 0 y SSD1306_HEIGHT - 1
 * @param  r: radio del círculo
 * @param  c: Color seleccionado desde @ref SSD1306_COLOR_t
 */
void SSD1306_DrawFilledCircle(int16_t x0, int16_t y0, int16_t r, SSD1306_COLOR_t c);

/** @} doxygen end group definition */


#ifndef ssd1306_I2C_TIMEOUT
#define ssd1306_I2C_TIMEOUT		20000
#endif

/**
 * @brief  Inicialización
 * @retval Estado de inicialización:
 *           - 0: No se detecta el display a través del I2C
 *           - > 0: Display inicializado y listo
 */
void ssd1306_I2C_Init();

/**
 * @brief  Envía un byte al dispositivo
 * @param  address: dirección de 7 bit del dispositivo
 * @param  reg: destino de la escritura
 * @param  data: datos envíados
 */
void ssd1306_I2C_Write(uint8_t address, uint8_t reg, uint8_t data);

/**
 * @brief  Envía varios bytes al dispositivo
 * @param  address: dirección de 7 bit del dispositivo
 * @param  reg: destino de la escritura
 * @param  *data: puntero que apunta hacia el vector de datos envíados
 * @param  count: contador de bytes envíados
 */
void ssd1306_I2C_WriteMulti(uint8_t address, uint8_t reg, uint8_t *data, uint16_t count);

/**
 * @brief  Imprime un bitmap
 * @param  x:  Valor de x inicial
 * @param  y:  Valor de y inicial
 * @param  *bitmap : Puntero que apunta al bitmap
 * @param  w : Largo
 * @param  h : Altura
 * @param  color : 1-> encendido, 0-> apagado
 */
void SSD1306_DrawBitmap(int16_t x, int16_t y, const unsigned char* bitmap, int16_t w, int16_t h, uint16_t color);

/**
 * @brief Desplaza la imagen del display hacia la derecha
 * @param start_row: Columna inicial
 * @param end_row  : Columa final
 */
void SSD1306_ScrollRight(uint8_t start_row, uint8_t end_row);

/**
 * @brief Desplaza la imagen del display hacia la izquierda
 * @param start_row: Columna inicial
 * @param end_row  : Columa final
 */
void SSD1306_ScrollLeft(uint8_t start_row, uint8_t end_row);

/**
 * @brief Desplaza la imagen del display hacia la derecha diagonal
 * @param start_row: Columna inicial
 * @param end_row  : Columa final
 */
void SSD1306_Scrolldiagright(uint8_t start_row, uint8_t end_row);

/**
 * @brief Desplaza la imagen del display hacia la izquierda diagonal
 * @param start_row: Columna inicial
 * @param end_row  : Columa final
 */
void SSD1306_Scrolldiagleft(uint8_t start_row, uint8_t end_row);

/**
 * @brief Detiene el desplazamiento
 */
void SSD1306_Stopscroll(void);

/**
 * @brief Invierte el display
 */
void SSD1306_InvertDisplay (int i);

/**
 * @brief Limpia el display
 */
void SSD1306_Clear (void);

/** @} doxygen end group definition */


/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/

#endif
