/*
 * fsm_blink.h
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 */

/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : fsm_blink.h
 * @brief          : Header para el archivo fsm_blink
 *                   Este archivo se encarga de las definiciones particulares para el control del LED
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 GRUPO 1 | TD2-FRH.
 * All rights reserved.</center></h2>
 ******************************************************************************
 */
/* USER CODE END Header */




#ifndef INC_FSM_BLINK_H_
#define INC_FSM_BLINK_H_

/*==================[inclusions]=============================================*/

#include "stm32f1xx_hal.h"
#include <stdbool.h>
#include <stdio.h>

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/


/** @addtogroup fsm_blink FSM Blink
 *			Este módulo se encarga de controlar los estados posibles del INDICADOR LUMINOSO
 * @{
 */

/** @brief <strong>Estados de los LED</strong>
 *
 * @note El indicador está apagado solo cuando la CAFETERA está en espera del pedido
 *  <a href="../html/group__fsm__cafetera.html#Estados_CAFETERA">(ESPERANDO_ELECCIóN)</a>
 * o finalizandolo <a href="../html/group__fsm__cafetera.html#Estados_CAFETERA">(SONIDO)</a>
 *
 */
typedef enum {
	FUNCIONANDO,		/**< El INDICADOR LUMINOSO parpadea a 1Hz durante este Estado*/
	EROGANDO,			/**< El INDICADOR LUMINOSO parpadea a 5Hz durante este Estado*/
	APAGADO				/**< El INDICADOR LUMINOSO se apaga durante este Estado*/
} FSM_BLINK_STATES_T;

/** @brief <strong>Estructura de las los indicadores luminosos</strong>
 *
 * Estructura parametrizable que permite el funcionamiento independiente de multiples cafeteras
 *
 * @note Las variables de tipo booleano se limpian cada runCycle
 *
 */
typedef struct{
	/** @brief <a href="../../media/Diagrama de estados.png">Estado del indicador luminoso</a>*/
	FSM_BLINK_STATES_T state;
	/** @brief Evento de blink a 1Hz del indicador luminoso */
	bool evBlink1Hz_On;

	/** @brief Evento de blink a 5Hz del indicador luminoso */
	bool evBlink5Hz_On;

	/** @brief Evento de de apagado de los LED*/
	bool evIndicadorLuz_Off;

	/** @brief Evento de blink a 1Hz para la erogación*/
	bool evToggle1Hz;

	/** @brief Evento de blink a 5Hz para la erogación*/
	bool evToggle5Hz;


	/** @brief Evento de blink a 5Hz para la erogación*/
	uint32_t idblink;

	/** @brief Contador para togglear a 1Hz*/
	uint32_t cont_500ms;

	/** @brief EContador para togglear a 5H*/
	uint32_t cont_100ms;
} FSM_BLINK_T;
/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @brief 	Inicialización de la máquina de eventos
 *			Se encarga de configurar al estado inicial de la CAFETERA y de limpiar todos los eventos
 *
 *	@note Los eventos se limpian al inicializar, y luego periodicamente al final de cada runCycle
 *
 */
void fsm_blink_init(void);

/** @brief Función periodica permanente de la maquina de estados<br>
 *         Esta función no deja de ejecutarse y es la encargada de administrar los
 *         estados de la CAFETERA
 *
 */
void fsm_blink_loop(void);

/** @brief Funcion llamada por HAL_IncTick para realizar los conteos
 *         Se encarga de manejar los eventos temporales periodicos como el blink de los diferentes LED
 *
 */
void fsm_blink_tick(void);



/** @defgroup evBlink_s Generación de eventos de la máquina de Estados
 *  Control del parpadeo del INDICADOR LUMINOSO de la CAFETERA
 *
 *  @note La frecuencia expresada en Hz incluye el prendido y apagado en en periodo de tiempo<br>
 *  Por ejemplo 1Hz significará que el LED permanece encendido 500 milisengundos
 *  y luego se apagaría por otros 500 milisegundos
 *	\par
 *  @note Las funciones raise solo se encargarán de poner en "1" las variables internas del programa<br>
 *  Estas son utilizadas en cada runCycle para conocer el estado de los pulsadores
 *  @{
 */

/** @brief Se envia señal para activar el indicador luminoso
 */
void fsm_blink_raise_evBlink1Hz_On(uint32_t id);

/** @brief Se envia señal para desactivar el indicador luminoso
 */
void fsm_blink_raise_evBlink5Hz_On(uint32_t id);

/** @brief Apagado del LED de indicador de funcionamiento
 */
void fsm_blink_raise_evIndicadorLuz_Off(uint32_t id);

/** @} */ // end of evBlink_s



/** @defgroup evBlink_a Control de las acciones de la máquina de Estados
 *
 *	Envío de pulsos para activar las acciones
 *
 *  @note Durante el estado de
 *  <a href="../html/group__fsm__cafetera.html#Estados_CAFETERA">(ESPERANDO_ELECCIóN)</a>
 *  el LED permanecera apagado o
 *  <a href="../html/group__fsm__cafetera.html#Estados_CAFETERA">(SONIDO)</a>
 *  \par
 *  @note <strong>id</strong> es la variable que corresponde al número de la cafetera
 *
 * @{
 */


/** @brief Se envia señal para togglear el indicador luminoso
 */
void fsm_blink_IndicadorLuz_Toggle(uint32_t id);

/** @brief Se envia señal para desactivar el indicador luminoso
 *
 * @note Debido a la falta de sincronismo de la máquina se necesita de esta función para asegurarse
 * del apagado del LED
 */
void fsm_blink_IndicadorLuz_Off(uint32_t id, GPIO_PinState status);

/** @} */ // end of evBlink_a

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* INC_FSM_BLINK_H_ */
