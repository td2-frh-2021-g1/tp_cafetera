/*
 * user_gpio.c
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 */

/*==================[inclusions]=============================================*/

#include "user_gpio.h"
#include "main.h"
#include "fsm_cafetera.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void user_gpio_init(void)
{
	fsm_cafetera_ErogacionTe_Off(FSM_1, STATUS_0);
	fsm_cafetera_ErogacionCafe_Off(FSM_1, STATUS_0);
	fsm_cafetera_IndicadorSonido_Off(FSM_1, STATUS_0);
	fsm_cafetera_ErogacionTe_Off(FSM_2, STATUS_0);
	fsm_cafetera_ErogacionCafe_Off(FSM_2, STATUS_0);
	fsm_cafetera_IndicadorSonido_Off(FSM_2, STATUS_0);
}

void user_gpio_loop(void)
{
	/* No se requiere ninguna tarea periódica porque los GPIO se leen mediante interrupciones */
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch (GPIO_Pin)
	{
	case FICHA_1_Pin:
		fsm_cafetera_raise_evFicha_On(FSM_1);
		break;
	case SENSOR_CAFE_1_Pin:
		fsm_cafetera_raise_evSensorCafe_On(FSM_1);
		break;
	case SENSOR_TE_1_Pin:
		fsm_cafetera_raise_evSensorTe_On(FSM_1);
		break;
	}
}


void fsm_cafetera_ErogacionTe_On(uint32_t id, GPIO_PinState status)
{
	switch (id)
	{
	case FSM_1:
		HAL_GPIO_WritePin(EROGAR_TE_1_GPIO_Port, EROGAR_TE_1_Pin, !status);
		break;
	case FSM_2:
		HAL_GPIO_WritePin(EROGAR_TE_2_GPIO_Port, EROGAR_TE_2_Pin, !status);
		break;
	}
}

void fsm_cafetera_ErogacionTe_Off(uint32_t id, GPIO_PinState status)
{
	switch (id)
	{
	case FSM_1:
		HAL_GPIO_WritePin(EROGAR_TE_1_GPIO_Port, EROGAR_TE_1_Pin, !status);
		break;
	case FSM_2:
		HAL_GPIO_WritePin(EROGAR_TE_2_GPIO_Port, EROGAR_TE_2_Pin, !status);
		break;
	}
}

void fsm_cafetera_ErogacionCafe_On(uint32_t id, GPIO_PinState status)
{
	switch (id)
	{
	case FSM_1:
		HAL_GPIO_WritePin(EROGAR_CAFE_1_GPIO_Port, EROGAR_CAFE_1_Pin, !status);
		break;
	case FSM_2:
		HAL_GPIO_WritePin(EROGAR_CAFE_2_GPIO_Port, EROGAR_CAFE_2_Pin, !status);
		break;
	}
}

void fsm_cafetera_ErogacionCafe_Off(uint32_t id, GPIO_PinState status)
{
	switch (id)
	{
	case FSM_1:
		HAL_GPIO_WritePin(EROGAR_CAFE_1_GPIO_Port, EROGAR_CAFE_1_Pin, !status);
		break;
	case FSM_2:
		HAL_GPIO_WritePin(EROGAR_CAFE_2_GPIO_Port, EROGAR_CAFE_2_Pin, !status);
		break;
	}
}

void fsm_cafetera_IndicadorSonido_On(uint32_t id, GPIO_PinState status)
{
	switch (id)
	{
	case FSM_1:
		HAL_GPIO_WritePin(SONIDO_1_GPIO_Port, SONIDO_1_Pin, !status);
		break;
	case FSM_2:
		HAL_GPIO_WritePin(SONIDO_2_GPIO_Port, SONIDO_2_Pin, !status);
		break;
	}
}

void fsm_cafetera_IndicadorSonido_Off(uint32_t id, GPIO_PinState status)
{
	switch (id)
	{
	case FSM_1:
		HAL_GPIO_WritePin(SONIDO_1_GPIO_Port, SONIDO_1_Pin, !status);
		break;
	case FSM_2:
		HAL_GPIO_WritePin(SONIDO_2_GPIO_Port, SONIDO_2_Pin, !status);
		break;
	}
}

void fsm_blink_IndicadorLuz_Toggle(uint32_t id)
{
	switch (id)
	{
	case FSM_1:
		HAL_GPIO_TogglePin(INDICADOR_1_GPIO_Port, INDICADOR_1_Pin);
		break;
	case FSM_2:
		HAL_GPIO_TogglePin(INDICADOR_2_GPIO_Port, INDICADOR_2_Pin);
		break;
	}
}

void fsm_blink_IndicadorLuz_Off(uint32_t id, GPIO_PinState status)
{
	switch (id)
	{
	case FSM_1:
		HAL_GPIO_WritePin(INDICADOR_1_GPIO_Port, INDICADOR_1_Pin, !status);
		break;
	case FSM_2:
		HAL_GPIO_WritePin(INDICADOR_2_GPIO_Port, INDICADOR_2_Pin, !status);
		break;
	}
}

void fsm_cafetera_devolverFicha(void)
{
	/* Do nothing */
}

/*==================[end of file]============================================*/
