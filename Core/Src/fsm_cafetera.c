/*
 * fsm_cafetera.c
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 */

/*==================[inclusions]=============================================*/

#define _GNU_SOURCE
#include <strings.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "fsm_cafetera.h"
#include "fsm_blink.h"
#include "user_gpio.h"
#include "esp8266.h"

/*==================[macros and definitions]=================================*/

#define ESPERA_USUARIO_SEG 5
#define EROGANDO_TE_SEG 7
#define EROGANDO_CAFE_SEG 10
#define INDICADOR_SONORO_SEG 2

/* Mensajes que se envían por la UART */
#define FICHA_MSJ_UART		"Maquina %u: Ingrese una ficha\r\n"
#define ELECCION_MSJ_UART 	"Maquina %u: ¿Quiere té o café?\r\n"
#define TE_MSJ_UART 		"Maquina %u: Tendrá su té listo en 7 segundos\r\n"
#define CAFE_MSJ_UART 		"Maquina %u: Tendrá su café listo en 10 segundos\r\n"
#define FINALIZADO_MSJ_UART "Maquina %u: ¡Ya puede disfrutar de su bebida!\r\n"

/* Mensajes que se envían por I2C */
#define FICHA_MSJ_I2C 		"Ingrese una ficha"
#define ELECCION_MSJ_I2C 	"Elija te o cafe  "
#define TE__MSJ_I2C 		"Ha elegido te!   "
#define CAFE_MSJ_I2C 		"Ha elegido cafe! "
#define FINALIZADO_MSJ_I2C 	"Finalizado       "

/* Strings a buscar cuando se recibe por la UART */
#define FICHA_STR 	"Ficha"
#define CAFE_STR 	"Cafe"
#define TE_STR 		"Te"

#define BUF_LEN 256

/*==================[internal data declaration]==============================*/

static uint8_t rx_buf[BUF_LEN];
static uint8_t tx_buf[BUF_LEN];

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

FSM_CAFETERA_T cafetera_1;
FSM_CAFETERA_T cafetera_2;

static uint32_t cont_ms;
static uint32_t cont_5ms;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(FSM_CAFETERA_T *handle)
{
	handle->evSensorTe_On = false;
	handle->evSensorCafe_On = false;
	handle->evFicha_On = false;
	handle->evTick1seg = false;
}

static void initFSM(FSM_CAFETERA_T *handle)
{
	fsm_cafetera_ErogacionTe_Off(handle->id, STATUS_0);
	fsm_cafetera_ErogacionCafe_Off(handle->id, STATUS_0);
	fsm_cafetera_IndicadorSonido_Off(handle->id, STATUS_0);
	handle->state = ESPERANDO_FICHA;
	handle->estado_enviado = false;
	clearEvents(handle);
}

static void runCycle(FSM_CAFETERA_T *handle)
{
	fsm_cafetera_raise_events(handle->id);

	switch (handle->state)
	{
	case ESPERANDO_FICHA:
		if (! handle->estado_enviado)
		{
			/* Envio mensajes por UART */
			snprintf((char *)tx_buf, BUF_LEN, FICHA_MSJ_UART, (uint32_t *)handle->id);
			esp8266_send_data(tx_buf, strlen((char *)tx_buf));
			/* Envio mensajes por I2C */
			handle->cont_5ms = INICIAL_CONT;
			if(handle->evTick5ms)
				lcd_print(FICHA_MSJ_I2C, handle->id);
			/* Mensaje enviado */
			handle->estado_enviado = true;
		}
		else if (handle->evFicha_On)
		{
			fsm_blink_raise_evIndicadorLuz_Off(handle->id);
			handle->cont_seg = INICIAL_CONT;
			handle->cont_ms = INICIAL_CONT;
			handle->estado_enviado = false;
			handle->state = ESPERANDO_ELECCION;
		}
		break;

	case ESPERANDO_ELECCION:
		if (! handle->estado_enviado)
		{
			/* Envio mensajes por UART */
			snprintf((char *)tx_buf, BUF_LEN, ELECCION_MSJ_UART, (uint32_t *)handle->id);
			esp8266_send_data(tx_buf, strlen((char *)tx_buf));
			/* Envio mensajes por I2C */
			lcd_print(ELECCION_MSJ_I2C, handle->id);
			/* Mensaje enviado */
			handle->estado_enviado = true;
		}
		else if (handle->evSensorTe_On)
		{
			fsm_blink_raise_evBlink5Hz_On(handle->id);
			fsm_cafetera_ErogacionTe_On(handle->id, STATUS_1);
			handle->cont_erogar_seg = INICIAL_CONT;
			handle->cont_ms = INICIAL_CONT;
			handle->estado_enviado = false;
			handle->state = EROGANDO_TE;
		}
		else if (handle->evSensorCafe_On)
		{
			fsm_blink_raise_evBlink5Hz_On(handle->id);
			fsm_cafetera_ErogacionCafe_On(handle->id, STATUS_1);
			handle->cont_erogar_seg = INICIAL_CONT;
			handle->cont_ms = INICIAL_CONT;
			handle->estado_enviado = false;
			handle->state = EROGANDO_CAFE;
		}
		else if (handle->evTick1seg && (handle->cont_seg < ESPERA_USUARIO_SEG))
		{
			handle->cont_seg ++;
		}
		else if(handle->cont_seg == ESPERA_USUARIO_SEG)
		{
			fsm_blink_raise_evBlink1Hz_On(handle->id);
			fsm_cafetera_devolverFicha();
			handle->estado_enviado = false;
			handle->state = ESPERANDO_FICHA;
		}
		break;

	case EROGANDO_TE:
		if (! handle->estado_enviado)
		{
			/* Envio mensajes por UART */
			snprintf((char *)tx_buf, BUF_LEN, TE_MSJ_UART, (uint32_t *)handle->id);
			esp8266_send_data(tx_buf, strlen((char *)tx_buf));
			/* Envio mensajes por I2C */
			lcd_print(TE__MSJ_I2C,handle->id);
			/* Mensaje enviado */
			handle->estado_enviado = true;
		}
		else if (handle->evTick1seg && (handle->cont_erogar_seg < EROGANDO_TE_SEG))
		{
			handle->cont_erogar_seg ++;
		}
		else if (handle->cont_erogar_seg == EROGANDO_TE_SEG)
		{
			fsm_blink_raise_evIndicadorLuz_Off(handle->id);
			fsm_cafetera_ErogacionTe_Off(handle->id, STATUS_0);
			fsm_cafetera_IndicadorSonido_On(handle->id, STATUS_1);
			handle->cont_sonido_seg = INICIAL_CONT;
			handle->cont_ms = INICIAL_CONT;
			handle->estado_enviado = false;
			handle->state = SONIDO;
		}
		break;

	case EROGANDO_CAFE:
		if (! handle->estado_enviado)
		{
			/* Envio mensajes por UART */
			snprintf((char *)tx_buf, BUF_LEN, CAFE_MSJ_UART, (uint32_t *)handle->id);
			esp8266_send_data(tx_buf, strlen((char *)tx_buf));
			/* Envio mensajes por I2C */
			lcd_print(CAFE_MSJ_I2C, handle->id);
			/* Mensaje enviado */
			handle->estado_enviado = true;
		}
		else if (handle->evTick1seg && (handle->cont_erogar_seg < EROGANDO_CAFE_SEG))
		{
			handle->cont_erogar_seg ++;
		}
		else if (handle->cont_erogar_seg == EROGANDO_CAFE_SEG)
		{
			fsm_blink_raise_evIndicadorLuz_Off(handle->id);
			fsm_cafetera_ErogacionCafe_Off(handle->id, STATUS_0);
			fsm_cafetera_IndicadorSonido_On(handle->id, STATUS_1);
			handle->cont_sonido_seg = INICIAL_CONT;
			handle->cont_ms = INICIAL_CONT;
			handle->estado_enviado = false;
			handle->state = SONIDO;
		}
		break;

	case SONIDO:
		if (! handle->estado_enviado)
		{
			/* Envio mensajes por UART */
			snprintf((char *)tx_buf, BUF_LEN, FINALIZADO_MSJ_UART, (uint32_t *)handle->id);
			esp8266_send_data(tx_buf, strlen((char *)tx_buf));
			/* Envio mensajes por I2C */
			lcd_print(FINALIZADO_MSJ_I2C,handle->id);
			/* Mensaje enviado */
			handle->estado_enviado = true;
		}
		else if (handle->evTick1seg && (handle->cont_sonido_seg < INDICADOR_SONORO_SEG))
		{
			handle->cont_sonido_seg ++;
		}
		else if (handle->cont_sonido_seg == INDICADOR_SONORO_SEG)
		{
			fsm_blink_raise_evBlink1Hz_On(handle->id);
			fsm_cafetera_IndicadorSonido_Off(handle->id, STATUS_0);
			handle->estado_enviado = false;
			handle->state = ESPERANDO_FICHA;
		}
		break;

	default:
		/* Do nothing */
		break;
	}

	clearEvents(handle);
}

/*==================[external functions definition]==========================*/

void fsm_cafetera_init(void)
{
	cafetera_1.id = FSM_1;
	initFSM(&cafetera_1);

	cafetera_2.id = FSM_2;
	initFSM(&cafetera_2);
}
void fsm_cafetera_loop(void)
{
	runCycle(&cafetera_1);
	runCycle(&cafetera_2);
}

void fsm_cafetera_tick(void)
{
	cont_ms ++;
	cont_5ms ++;
	if (cont_5ms >= 5)
	{
		cont_5ms = INICIAL_CONT;
		cafetera_1.evTick5ms= true;
		cafetera_2.evTick5ms = true;
	}
	if (cont_ms >= CONT_1000ms)
	{
		cont_ms = INICIAL_CONT;
		cafetera_1.evTick1seg = true;
		cafetera_2.evTick1seg = true;
	}
}

void fsm_cafetera_raise_events(uint32_t id)
{
	if (id == FSM_2)
	{
		esp8266_recv_data(rx_buf, BUF_LEN);

		if (memmem(rx_buf, BUF_LEN, FICHA_STR, strlen(FICHA_STR)))
		{
			fsm_cafetera_raise_evFicha_On(FSM_2);
		}
		if (memmem(rx_buf, BUF_LEN, CAFE_STR, strlen(CAFE_STR)))
		{
			fsm_cafetera_raise_evSensorCafe_On(FSM_2);
		}
		if (memmem(rx_buf, BUF_LEN, TE_STR, strlen(TE_STR)))
		{
			fsm_cafetera_raise_evSensorTe_On(FSM_2);
		}
	}

	bzero(rx_buf, BUF_LEN);
}

void fsm_cafetera_raise_evSensorTe_On(uint32_t id)
{
	switch (id)
	{
	case FSM_1:
		cafetera_1.evSensorTe_On = true;
		break;
	case FSM_2:
		cafetera_2.evSensorTe_On = true;
		break;
	}
}

void fsm_cafetera_raise_evSensorCafe_On(uint32_t id)
{
	switch (id)
	{
	case FSM_1:
		cafetera_1.evSensorCafe_On = true;
		break;
	case FSM_2:
		cafetera_2.evSensorCafe_On = true;
		break;
	}
}

void fsm_cafetera_raise_evFicha_On(uint32_t id)
{
	switch (id)
	{
	case FSM_1:
		cafetera_1.evFicha_On = true;
		break;
	case FSM_2:
		cafetera_2.evFicha_On = true;
		break;
	}
}

/*==================[end of file]============================================*/
