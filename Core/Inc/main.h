/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header para el archivo main
 *                   Este archivo se encarga de las declaraciones comunes de la aplicación
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 GRUPO 1 | TD2-FRH.
 * All rights reserved.</center></h2>
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

extern UART_HandleTypeDef huart1;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define INDICADOR_1_Pin GPIO_PIN_13
#define INDICADOR_1_GPIO_Port GPIOC
#define EROGAR_CAFE_2_Pin GPIO_PIN_4
#define EROGAR_CAFE_2_GPIO_Port GPIOA
#define EROGAR_TE_2_Pin GPIO_PIN_5
#define EROGAR_TE_2_GPIO_Port GPIOA
#define SONIDO_2_Pin GPIO_PIN_6
#define SONIDO_2_GPIO_Port GPIOA
#define INDICADOR_2_Pin GPIO_PIN_7
#define INDICADOR_2_GPIO_Port GPIOA
#define EROGAR_CAFE_1_Pin GPIO_PIN_0
#define EROGAR_CAFE_1_GPIO_Port GPIOB
#define EROGAR_TE_1_Pin GPIO_PIN_1
#define EROGAR_TE_1_GPIO_Port GPIOB
#define SONIDO_1_Pin GPIO_PIN_10
#define SONIDO_1_GPIO_Port GPIOB
#define SENSOR_TE_1_Pin GPIO_PIN_12
#define SENSOR_TE_1_GPIO_Port GPIOB
#define SENSOR_TE_1_EXTI_IRQn EXTI15_10_IRQn
#define SENSOR_CAFE_1_Pin GPIO_PIN_13
#define SENSOR_CAFE_1_GPIO_Port GPIOB
#define SENSOR_CAFE_1_EXTI_IRQn EXTI15_10_IRQn
#define FICHA_1_Pin GPIO_PIN_14
#define FICHA_1_GPIO_Port GPIOB
#define FICHA_1_EXTI_IRQn EXTI15_10_IRQn
#define ESP_ENABLE_Pin GPIO_PIN_15
#define ESP_ENABLE_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
