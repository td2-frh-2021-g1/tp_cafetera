/*
 * fsm_cafetera.h
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 */

/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : fsm_cafetera.h
 * @brief          : Header para el archivo fsm_cafetera
 *                   Este archivo se encarga de las definiciones particulares para el control de la erogación
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 GRUPO 1 | TD2-FRH.
 * All rights reserved.</center></h2>
 ******************************************************************************
 */
/* USER CODE END Header */




#ifndef INC_FSM_CAFETERA_H_
#define INC_FSM_CAFETERA_H_

/*==================[inclusions]=============================================*/

#include "stm32f1xx_hal.h"
#include <stdbool.h>
#include <stdio.h>

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

#define FSM_1 1
#define FSM_2 2
#define STATUS_0 0
#define STATUS_1 1

#define INICIAL_CONT 0
#define CONT_100ms 100
#define CONT_500ms 500
#define CONT_1000ms 1000


/*==================[typedef]================================================*/

/** @addtogroup fsm_cafetera FSM Cafetera
 *			Este módulo se encarga de controlar los estados posibles de la CAFETERA
 * <a id="Estados_CAFETERA"></a>
 * @{
 */

/** @brief <strong>Estados de la CAFETERA</strong>
 *
 * @note No realizar una elección en el estado ESPERANDO_ELECCIÓN resultará en la devolución de la ficha
 * y el regreso al estado de ESPERANDO_FICHA
 *
 */
typedef enum{
	ESPERANDO_FICHA,		/**< El INDICADOR LUMINOSO parpadea a 1Hz durante este Estado */

	ESPERANDO_ELECCION,		/**< El INDICADOR LUMINOSO se apaga durante este Estado <br>
							<ins>Duraciòn estimada de 5 segundos </ins> */

	EROGANDO_TE,			/**< El INDICADOR LUMINOSO parpadea a 5Hz durante este Estado <br>
							<ins>Duraciòn estimada de 7 segundos </ins> */

	EROGANDO_CAFE,			/**< El INDICADOR LUMINOSO parpadea a 5Hz durante este Estado <br>
							<ins>Duraciòn estimada de 10 segundos </ins> */

	SONIDO					/**< El INDICADOR LUMINOSO se apaga durante este Estado <br>
							<ins>Duraciòn estimada de 2 segundos </ins> */

} FSM_CAFETERA_STATES_T;

typedef struct{
	FSM_CAFETERA_STATES_T state;
	bool evSensorTe_On;
	bool evSensorCafe_On;
	bool evFicha_On;
	bool evTick1seg;
	bool evTick5ms;
	bool estado_enviado;
	uint32_t cont_ms;
	uint32_t id;
	uint8_t cont_seg;
	uint8_t cont_5ms;
	uint8_t cont_erogar_seg;
	uint8_t cont_sonido_seg;
} FSM_CAFETERA_T;
/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @brief Inicialización de la máquina de estados
 *
 *			Se encarga de configurar al estado inicial de la CAFETERA y de limpiar todos los eventos
 *
 *	@note Los eventos se limpian al inicializar, y luego periodicamente al final de cada runCycle
 *
 */
void fsm_cafetera_init();/****************************************************************/

/** @brief Función periodica permanente de la maquina de estados
 *
 *         Esta función no deja de ejecutarse y es la encargada de administrar los estados de la CAFETERA
 */
void fsm_cafetera_loop(void);/************************************************************/

/** @brief Funcion llamada por HAL_IncTick para realizar los conteos*/
void fsm_cafetera_tick(void);

/** @defgroup Eventos Generación de eventos de la máquina de Estados
 *
 * Control de los estados de la CAFETERA
 *
 * @note Todas estas funciones se limpian periodicamente, tras cada runCycle
 * \par
 * @note Las funciones raise solo se encargarán de poner en "1" las variables internas del programa<br>
 * Estas son utilizadas en cada runCycle para conocer el estado de los pulsadores
 * @{
 *
 *
 */

/** @brief Esta función permite activar los eventos de la máquina 2
 *  @param id
 *  @return None
 *
 */
void fsm_cafetera_raise_events(uint32_t id);

/** @brief Sensor TE activado
 *  @param id
 *  @return None
 *
 */
void fsm_cafetera_raise_evSensorTe_On(uint32_t id);

/** @brief Sensor CAFÉ activado
 *  @param id
 *  @return None
 *
 */
void fsm_cafetera_raise_evSensorCafe_On(uint32_t id);

/** @brief Ficha introducida
 *  @param id
 *  @return None
 *
 */
void fsm_cafetera_raise_evFicha_On(uint32_t id);
/** @} */ // end of Eventos

/** @defgroup erogacion Control de las acciones del la máquina de Estados
 *
 * Envío de pulsos para activar las acciones
 *
 * @{
 */
/** @brief Se envia señal para activar la elección de Té
 *  @param id
 *  @return None
 *
 */
void fsm_cafetera_ErogacionTe_On(uint32_t id, GPIO_PinState status);

/** @brief Se envia señal para desactivar la elección de Té
 *  @param id
 *  @param status
 *  @return None
 *
 */
void fsm_cafetera_ErogacionTe_Off(uint32_t id, GPIO_PinState status);

/** @brief Se envia señal para activar la elección de CAFé
 *  @param id
 *  @param status
 *  @return None
 *
 */
void fsm_cafetera_ErogacionCafe_On(uint32_t id, GPIO_PinState status);

/** @brief Se envia señal para desactivar la elección de CAFé
 *  @param id
 *  @param status
 *  @return None
 *
 */
void fsm_cafetera_ErogacionCafe_Off(uint32_t id, GPIO_PinState status);

/** @brief Se envia señal para activar el indicador sonoro
 *  @param id
 *  @param status
 *  @return None
 *
 */
void fsm_cafetera_IndicadorSonido_On(uint32_t id, GPIO_PinState status);

/** @brief Se envia señal para desactivar el indicador sonoro
 *  @param id
 *  @param status
 *  @return None
 *
 */
void fsm_cafetera_IndicadorSonido_Off(uint32_t id, GPIO_PinState status);

/** @brief Se envia señal para devolver la ficha
 *
 */
void fsm_cafetera_devolverFicha(void);
/** @} */ // end of erogacion

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* INC_FSM_CAFETERA_H_ */
