/*
 * esp8266.c
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 */

/*==================[inclusions]=============================================*/

#define _GNU_SOURCE
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <stdio.h>

#include "main.h"
#include "fsm_cafetera.h"
#include "esp8266.h"

/*==================[macros and definitions]=================================*/

#define RX_BUF_LEN 256
#define AUX_BUF_LEN 256
#define USER_BUF_LEN 256
#define INITIAL_COUNT_MS 100
#define FINAL_COUNT_MS 0
#define DELAY_INIT_MS 500

#define ERROR -1

/* Comandos AT*/
#define CWMODE_STR "AT+CWMODE=3\r\n"
#define CWJAP_STR "AT+CWJAP=\"REDTEST\",\"alejo2021\"\r\n"
#define CIPSTART_STR "AT+CIPSTART=\"UDP\",\"192.168.43.223\",8000,8001\r\n"
#define CIPSEND_STR "AT+CIPSEND=%u\r\n"

/* Strings a esperar */
#define READY "ready\r\n"
#define OK "OK\r\n"
#define WIFI_CON "WIFI CONNECTED\r\n"
#define WIFI_GOT "WIFI GOT IP\r\n"
#define CONNECT "CONNECT\r\n"
#define SEND_OK "SEND OK\r\n"
#define SIGNO "> "
#define IPD "+IPD,"

/*==================[internal data declaration]==============================*/

static FSM_ESP8266_STATES_T state;

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

static uint32_t counter_ms;
static uint32_t longitud;

static uint8_t rx_buf[RX_BUF_LEN];
static uint8_t aux_buf[AUX_BUF_LEN];
static uint8_t user_tx_buf[USER_BUF_LEN];
static uint8_t user_rx_buf[USER_BUF_LEN];
static uint8_t counter_tx_done;

static bool tx_done = false;
static bool rx_done = false;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clean_rx_buffer(void)
{
	huart1.pRxBuffPtr = rx_buf;
	huart1.RxXferSize = RX_BUF_LEN;
	huart1.RxXferCount = RX_BUF_LEN;
	bzero(rx_buf, RX_BUF_LEN);
}

/*==================[external functions definition]==========================*/

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	tx_done = true;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	rx_done = true;
}

void esp8266_tick(void)
{
	if (counter_ms > 0)
	{
		counter_ms--;
	}
}

void esp8266_init(void)
{
	state = ESPERO_READY;
	clean_rx_buffer();
	bzero(user_tx_buf, USER_BUF_LEN);

	HAL_GPIO_WritePin(ESP_ENABLE_GPIO_Port, ESP_ENABLE_Pin, GPIO_PIN_RESET);
	HAL_Delay(DELAY_INIT_MS);
	HAL_GPIO_WritePin(ESP_ENABLE_GPIO_Port, ESP_ENABLE_Pin, GPIO_PIN_SET);
}

void esp8266_loop(void)
{
	HAL_UART_Receive_IT(&huart1, rx_buf, RX_BUF_LEN);

	switch(state)
	{

	/* Proceso de conexión */
	case ESPERO_READY:
		if (memmem(rx_buf, RX_BUF_LEN, READY, strlen(READY)))
		{
			clean_rx_buffer();
			counter_ms = INITIAL_COUNT_MS;
			state = ENVIO_CWMODE;
		}
		break;

	case ENVIO_CWMODE:
		if (counter_ms == FINAL_COUNT_MS)
		{
			HAL_UART_Transmit_IT(&huart1, (uint8_t *)CWMODE_STR, strlen(CWMODE_STR));
			clean_rx_buffer();;
			tx_done = false;
			counter_tx_done = 1;
			state = TX_DONE;
		}
		break;

	case ENVIO_CWJAP:
		if (memmem(rx_buf, RX_BUF_LEN, OK, strlen(OK)))
		{
			HAL_UART_Transmit_IT(&huart1, (uint8_t *)CWJAP_STR, strlen(CWJAP_STR));
			clean_rx_buffer();
			tx_done = false;
			counter_tx_done = 2;
			state = TX_DONE;
		}
		break;

	case ENVIO_CIPSTART:
		if (memmem(rx_buf, RX_BUF_LEN, OK, strlen(OK)) &&
			memmem(rx_buf, RX_BUF_LEN, WIFI_CON, strlen(WIFI_CON)) &&
			memmem(rx_buf, RX_BUF_LEN, WIFI_GOT, strlen(WIFI_GOT)))
		{
			HAL_UART_Transmit_IT(&huart1, (uint8_t *)CIPSTART_STR, strlen(CIPSTART_STR));
			clean_rx_buffer();
			tx_done = false;
			counter_tx_done = 3;
			state = TX_DONE;
		}
		break;

	case CONEXION_OK:
		if (memmem(rx_buf, RX_BUF_LEN, OK, strlen(OK)) &&
			memmem(rx_buf, RX_BUF_LEN, CONNECT, strlen(CONNECT)))
		{
			clean_rx_buffer();
			tx_done = false;
			state = ENVIO_Y_RECIBO;
		}
		break;

	/* Transmisión y recepción de datos */
	case ENVIO_Y_RECIBO:
		if (user_tx_buf[0])
		{
			snprintf((char *)aux_buf, RX_BUF_LEN, CIPSEND_STR, strlen((char *)user_tx_buf));
			HAL_UART_Transmit_IT(&huart1, (uint8_t *)aux_buf, strlen((char *)aux_buf));
			clean_rx_buffer();
			tx_done = false;
			counter_tx_done = 4;
			state = TX_DONE;
		}
		if (memmem(rx_buf, RX_BUF_LEN, IPD, strlen(IPD))){
			memcpy(user_rx_buf, rx_buf, RX_BUF_LEN);
			clean_rx_buffer();
			rx_done = false;
		}
		break;

	case ENVIO_MENSAJE:
		if (memmem(rx_buf, RX_BUF_LEN, SIGNO, strlen(SIGNO)))
		{
			HAL_UART_Transmit_IT(&huart1, (uint8_t *)user_tx_buf, strlen((char *)user_tx_buf));
			clean_rx_buffer();
			tx_done = false;
			counter_tx_done = 5;
			state = TX_DONE;
		}
		break;

	case ESPERO_SEND_OK:
		if (memmem(rx_buf, RX_BUF_LEN, SEND_OK, strlen(SEND_OK)))
		{
			clean_rx_buffer();
			bzero(user_tx_buf, USER_BUF_LEN);
			counter_ms = DELAY_INIT_MS;
			tx_done = false;
			counter_tx_done = 6;
			state = TX_DONE;
		}
		break;

	/* tx_done */
	case TX_DONE:
		if (tx_done && counter_tx_done == 1)
		{
			state = ENVIO_CWJAP;
		}
		else if (tx_done && counter_tx_done == 2)
		{
			state = ENVIO_CIPSTART;
		}
		else if (tx_done && counter_tx_done == 3)
		{
			state = CONEXION_OK;
		}
		else if (tx_done && counter_tx_done == 4)
		{
			state = ENVIO_MENSAJE;
		}
		else if (tx_done && counter_tx_done == 5)
		{
			state = ESPERO_SEND_OK;
		}
		else if (counter_tx_done == 6 && counter_ms == 0)
		{
			state = ENVIO_Y_RECIBO;
		}
		break;

	default:
		/* Do nothing */
		break;
	}
}

int32_t esp8266_send_data(void * data, size_t len)
{
	if (user_tx_buf[0])
	{
		return ERROR;
	}
	else
	{
		memcpy(user_tx_buf, data, len > USER_BUF_LEN ? USER_BUF_LEN : len);
		longitud = strlen(data);
		return longitud;
	}
}

int32_t esp8266_recv_data(void * data, size_t len)
{
	if (user_rx_buf[0])
	{
		memcpy(data, user_rx_buf, len > USER_BUF_LEN ? USER_BUF_LEN : len);
		bzero(user_rx_buf, USER_BUF_LEN);
		longitud = strlen(data);
		return longitud;
	}
	else
	{
		return ERROR;
	}
}

/*==================[end of file]============================================*/
