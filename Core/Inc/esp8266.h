/*
 * esp8266.h
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 */

/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : esp8266.h
 * @brief          : Header para el archivo esp8266
 *                   Este archivo es el driver para la ESP8266 para el control de la comunicación Wifi
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 GRUPO 1 | TD2-FRH.
 * All rights reserved.</center></h2>
 ******************************************************************************
 */
/* USER CODE END Header */

#ifndef INC_ESP8266_H_
#define INC_ESP8266_H_

/** @addtogroup esp8266 ESP8266
 *			Este módulo se encarga de configurar la conexión del dispositivo Wifi ESP8266,
 *			enviar los mensajes de estado de la cafetera y recibir.
 * @{
 */

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <stddef.h>

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/** @brief <strong>Estados de la ESP8266</strong>
 */

typedef enum{
	ESPERO_READY,		/**< Busca recibir un "ready" por la UART para poder empezar a enviar datos*/
	ESPERO_SEND_OK,		/**< Busca recibir un "SEND OK" por la UART para poder enviar nuevamente datos*/
	ENVIO_CWMODE,		/**< Configura el modo de operación como cliente*/
	ENVIO_CWJAP,		/**< Envía la red y password para establecer conexión con la ESP*/
	ENVIO_CIPSTART,		/**< Inicia una conexión UDP hacia la IP y el puerto remoto disponible*/
	ENVIO_CIPSEND,		/**< Envía la cantidad de bytes que vamos a transmitir*/
	ENVIO_MENSAJE,		/**< Envía el mensaje a transmitir a través de la ESP*/
	CONEXION_OK,		/**< Busca corroborar la correcta conexión con la ESP*/
	ENVIO_Y_RECIBO,		/**< Estado comùn para envío y recepción*/
	TX_DONE				/**< Corrobora que la tranmisión finalizó para proseguir*/
} FSM_ESP8266_STATES_T;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @brief 	<b>Inicialización del módulo Wifi y la máquina de estados</b>
 * @note	Se encarga de:
 * 				- Habilitar el chip ESP8266
 * 				- Limpiar los buffers de recepción y transmisión de datos del usuario
 * 				- Inicializar el primer estado de la máquina
 */
void esp8266_init(void);

/** @brief	<b>Función llamada por HAL_IncTick para realizar los conteos temporales</b>
 *  @note	Se encarga de:
 *  			- Realizar en este caso, un contador decreciente
 *  			- Esperamos 100ms para poder empezar a transmitir los comandos AT
 *  			para establecer la conexión con la ESP
 */
void esp8266_tick(void);

/** @brief 	<b>Función periodica permanente de la maquina de estados</b>
 *	@note	Se encarga de:
 *				- Administrar los estados de la FSM
 *				- Establecer la conexión Wifi con la ESP
 *				- Enviar/recibir datos por la UART de la Bluepill
 */
void esp8266_loop(void);

/**
 * @brief	<b>Función que envía el mensaje a transmitir y su tamaño</b>
 *
 * @param 	data Guarda la información a envìar
 * @param 	len Tamaño del dato
 * @return
 * 			- "-1" cuando todavia hay un dato enviándose
 * 			- "0" cuando el mensaje se copia en el buffer de transmisión
 */
int32_t esp8266_send_data(void * data, size_t len);

/**
 * @brief	<b>Función que recibe los datos desde la ESP y su tamaño</b>
 *
 * @param 	data Guarda la información recibida
 * @param 	len Tamaño del dato
 * @return
 * 			- "-1" cuando todavia hay un dato recibiéndose
 * 			- "0" cuando el mensaje se copia en el buffer de recepción
 */
int32_t esp8266_recv_data(void * data, size_t len);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/

#endif /* INC_ESP8266_H_ */
