/*
 * user_gpio.h
 *
 *      Author: GRUPO 1 TD2, 2021
 *
 * 		Universidad Tecnologica Nacional
 * 		Facultad Regional Haedo
 * 		Tecnicas Digitales II
 *
 */

/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : user_gpio.h
 * @brief          : Header para el archivo user_gpio
 *                   Este archivo se encarga de las definiciones particulares para el control de los GPIOs de la Blue Pill.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 GRUPO 1 | TD2-FRH.
 * All rights reserved.</center></h2>
 ******************************************************************************
 */
/* USER CODE END Header */

#ifndef INC_USER_GPIO_H_
#define INC_USER_GPIO_H_

/** @addtogroup user_gpio Uso de los GPIOs
 * 				Configuración inicial de los puertos de la bluepill
 * @{
 */

/*==================[inclusions]=============================================*/

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @brief Inicialización de los GPIOs
 *
 * @note Es necesario inicializar en "0" los eventos de ambas máquinas de estado
 */
void user_gpio_init(void);

/** @brief Función periódica que evalúa el estado de las entradas GPIO
 *
 * @warning Se opta por interrupciones por lo cual no se utiliza funciones periódicas para las señales
 * de entrada
 */
void user_gpio_loop(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* INC_USER_GPIO_H_ */
